$(function(){
  $("[data-toggle='popover']").tooltip();
  $("[data-toggle='popover']").popover();
});   

$('#sugerencia').on('show.bs.modal', function (e) {
  // do something...
  alert("Se mostró el modal");
  // La siguiente linea desabilita el boton despues de abrir el Modal 
  // Ya que estamos usando el evento posterior SHOW 
  $('#sugerir').removeClass('btn-primary');
  $('#sugerir').addClass('btn-outline-success');

  $('#sugerir').prop('disabled', true);
  // se puede volver a habilitar colocando FALSE en un evento HIDDEN O SHOWN 
  // ----------------------------------------------------
});

$('#sugerencia').on('hidden.bs.modal', function (e) {
   $('#sugerir').prop('disabled', false);
});
  